package com.veinhorn.scrollgalleryview;

import com.veinhorn.scrollgalleryview.loader.MediaLoader;

/**
 * MediaInfo contains the information required to load and display the media in the gallery.
 */
public class MediaInfo {
    private MediaLoader mLoader;
    private String description;
    private Object obj;

    private MediaInfo(Object obj) {
        this.obj = obj;
    }

    private MediaInfo(MediaLoader mediaLoader, String description, Object obj) {
        this.mLoader = mediaLoader;
        this.description = description;
        this.obj = obj;
    }

    /**
     * Creates MediaInfo object from MediaLoader
     *
     * @param mediaLoader
     * @return MediaInfo object
     */
    public static MediaInfo mediaLoader(Object obj, MediaLoader mediaLoader) {
        return new MediaInfo(obj).setLoader(mediaLoader);
    }

    public static MediaInfo mediaLoader(MediaLoader mediaLoader, String description,Object obj) {
        return new MediaInfo(mediaLoader, description,obj);
    }

    public MediaLoader getLoader() {
        return mLoader;
    }

    public MediaInfo setLoader(MediaLoader loader) {
        mLoader = loader;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Object getObj() {
        return obj;
    }
}
