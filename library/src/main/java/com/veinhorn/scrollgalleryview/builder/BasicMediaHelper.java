package com.veinhorn.scrollgalleryview.builder;

import com.veinhorn.scrollgalleryview.MediaInfo;
import com.veinhorn.scrollgalleryview.loader.DefaultVideoLoader;

/**
 * Basic implementation of MediaHelper which supports adding video
 */
public abstract class BasicMediaHelper implements MediaHelper {
    @Override
    public MediaInfo video(String url, int placeholderViewId, Object obj) {
        return MediaInfo.mediaLoader(obj, new DefaultVideoLoader(url, placeholderViewId));
    }
}
