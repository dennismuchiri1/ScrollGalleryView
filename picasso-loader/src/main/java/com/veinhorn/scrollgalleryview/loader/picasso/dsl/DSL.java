package com.veinhorn.scrollgalleryview.loader.picasso.dsl;

import com.veinhorn.scrollgalleryview.MediaInfo;
import com.veinhorn.scrollgalleryview.loader.picasso.PicassoMediaHelper;

import java.util.List;

public class DSL {
    private static PicassoMediaHelper mediaHelper = new PicassoMediaHelper();

    public static MediaInfo image(String url,Object obj) {
        return mediaHelper.image(url,obj);
    }

    public static MediaInfo image(String url, String description,Object obj) {
        return mediaHelper.image(url, description,obj);
    }

    public static List<MediaInfo> images(List<Object>objs,List<String> urls) {
        return mediaHelper.images(urls,objs);
    }

    public static List<MediaInfo> images(List<Object>objs,String... urls) {
        return mediaHelper.images(objs,urls);
    }

    public static MediaInfo video(String url, int placeholderViewId,Object obj) {
        return mediaHelper.video(url, placeholderViewId,obj);
    }
}
