package ogbe.ozioma.com.glideimageloader;

import com.veinhorn.scrollgalleryview.MediaInfo;
import com.veinhorn.scrollgalleryview.builder.BasicMediaHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GlideMediaHelper extends BasicMediaHelper {
    @Override
    public MediaInfo image(String url, Object obj) {
        return MediaInfo.mediaLoader(obj, new GlideImageLoader(url));
    }

    @Override
    public MediaInfo image(String url, String description, Object obj) {
        return mediaInfo(url, description, obj);
    }

    @Override
    public List<MediaInfo> images(List<String> urls,List<Object> objs) {
        List<MediaInfo> medias = new ArrayList<>();

        /*for (String url : urls) {
            medias.add(mediaInfo(url));
        }*/
        for(int i=0;i<urls.size();i++){
            String url=urls.get(i);
            Object obj=objs.get(i);
            medias.add(mediaInfo(url,obj));
        }

        return medias;
    }

    @Override
    public List<MediaInfo> images(List<Object>objs,String... urls) {
        return images(Arrays.asList(urls),objs);
    }

    private MediaInfo mediaInfo(String url, Object obj) {
        return mediaInfo(url, obj);
    }

    private MediaInfo mediaInfo(String url, String description, Object obj) {
        if (description != null) {
            return MediaInfo.mediaLoader(new GlideImageLoader(url), description, obj);
        }
        return MediaInfo.mediaLoader(obj, new GlideImageLoader(url));
    }
}
