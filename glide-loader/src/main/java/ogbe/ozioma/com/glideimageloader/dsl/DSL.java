package ogbe.ozioma.com.glideimageloader.dsl;

import com.veinhorn.scrollgalleryview.MediaInfo;

import java.util.List;

import ogbe.ozioma.com.glideimageloader.GlideMediaHelper;

public class DSL {
    private static GlideMediaHelper mediaHelper = new GlideMediaHelper();

    public static MediaInfo image(String url,Object obj) {
        return mediaHelper.image(url,obj);
    }

    public static MediaInfo image(String url, String description) {
        return mediaHelper.image(url, description);
    }

    public static List<MediaInfo> images(List<String> urls,List<Object> objs) {
        return mediaHelper.images(urls,objs);
    }

    public static List<MediaInfo> images(List<Object> objs,String... urls) {
        return mediaHelper.images(objs,urls);
    }

    public static MediaInfo video(String url, int placeholderViewId,Object obj) {
        return mediaHelper.video(url, placeholderViewId,obj);
    }
}
