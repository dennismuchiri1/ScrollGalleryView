package com.ayokunlepaul.frescoloader.dsl;

import com.ayokunlepaul.frescoloader.FrescoMediaHelper;
import com.veinhorn.scrollgalleryview.MediaInfo;

import java.util.List;

public class DSL {
    private static FrescoMediaHelper mediaHelper = new FrescoMediaHelper();

    public static MediaInfo image(String url,Object obj) {
        return mediaHelper.image(url,obj);
    }

    public static MediaInfo image(String url, String description) {
        return mediaHelper.image(url, description);
    }

    public static List<MediaInfo> images(List<Object> objs,List<String> urls) {
        return mediaHelper.images(urls,objs);
    }

    public static List<MediaInfo> images(List<Object> objs,String... urls) {
        return mediaHelper.images(objs,urls);
    }

    public static MediaInfo video(String url, int placeholderViewId,Object obj) {
        return mediaHelper.video(url, placeholderViewId,obj);
    }
}
